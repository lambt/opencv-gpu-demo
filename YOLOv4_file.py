""""
OPENCV YOLO v4 web cam object detection with  GPU/CPU

https://gist.github.com/YashasSamaga/e2b19a6807a13046e399f4bc3cca3a49
https://github.com/easyadin/Object-Detection-YOLOv4

"""
import cv2
import time
import numpy as np

CONFIDENCE_THRESHOLD = 0.2
NMS_THRESHOLD = 0.4

net = cv2.dnn.readNet("./model_YOLOv4/yolov4.weights", "./model_YOLOv4/yolov4.cfg")

# comment to make inference by CPU cores :
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)

# if your device supports FP16 replace this with the above line:
# net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA_FP16)

model = cv2.dnn_DetectionModel(net)
model.setInputParams(size=(416, 416), scale=1 / 255, swapRB=True)

with open("./model_YOLOv4/coco.names", "r") as f:
    class_names = [cname.strip() for cname in f.readlines()]

cap = cv2.VideoCapture("0.mp4")

start = time.time()
count = 0.0
while True:
    ret, frame = cap.read()
    count += 1
    if not ret:
        print(' stream gone yaw !')
        break
    classes, scores, boxes = model.detect(frame, CONFIDENCE_THRESHOLD, NMS_THRESHOLD)
duration = time.time() - start
cap.release()

print(f"AVG FPS: {count/duration}")